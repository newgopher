#!/usr/bin/perl

use strict;
use warnings;

use IO::Socket::SSL;

package TLSGopher;
#  openssl s_client -connect localhost:11211

our $VERSION = '0.01';

use base 'Exporter';
our @EXPORT = qw(ng_parse_uri ng_parse_menu ng_parse_response);
#
# UTILS
#
sub ng_parse_uri {
	my $uri = $_[0] || '';
	$uri =~ s/gopher:\/\///;
	chomp($uri);
	my ($long_host, $selector) = split /\//, $uri;
	my ($host, $port) = split /:/, $long_host;
	$port = $port || 32070; # default port
	undef $long_host;
	return ($host, $port, $selector);
}

sub ng_parse_menu {
	my ($data, $host, $port) = @_;
	my @items = split /\r\n/, $data;
	my $i = 0;
	my @menu;
	foreach (@items) {
		my ($mtype, $name, $selector, $_host, $_port) = split /\t/;
		if (!$_port || !($_port =~ /\d+/)) { $_port = 0; }
		$menu[$i] = {
			'name' => $name,
			'type' => ng_shorten_type($mtype),
			'mimetype' => ng_expand_type($mtype),
			'selector' => $selector, 
			'host' => ($_host ? $_host : $host), 
			'port' => ($_port ? int($_port) : $port),
		};
		$i++;
	}
	return @menu;
}

sub ng_parse_response {
	my $data = $_[0];
	my $size = 0;
	my $mimetype = 'unknown';
	if ($data =~ /(-1|\d+)\t(.+)\r/) {
		$size = $1;
		$mimetype = $2;
	}
	$data =~ s/.+\r\n//;
	return ($size, $mimetype, $data);
}

sub ng_expand_type {
	my $type = shift;
	return 'text/x-menu' if ($type eq 'm');
	return 'application/octet-stream' if ($type eq 'b');
	return '';
}

sub ng_shorten_type {
	my $type = shift;
	return $type if ($type =~ /i|m|s|b|u/);
	return 'i' if ($type eq '');
	return 'm' if ($type eq 'text/x-menu');
	return 's' if ($type eq 'application/x-interactive');
	return 'b';
}

#
# REQUEST
#
{#<package>
package TLSGopher::request;

sub new {
	my ($class, $host, $port, $selector) = @_;
	my $self = {};
	bless $self;

	$self->{errstr} = '';

	$self->{host} = $host;
	$self->{port} = $port;
	$self->{selector} = $selector;

	$self->clear;
	$self->{post_fd} = undef;

	$self->{ready} = 0;
	$self->{sent} = 0;

	return $self;
}

sub parse1 {
	my $self = shift;
	my $line = shift;

	$self->{'buf'.'HEADER1'} = $line;

	$line =~ s/\r\n//m;

	($self->{selector},
	$self->{search},
	$self->{range})  = split(/\t/, $line);

	# Request is complex
	return 1 if ($line =~ /\t/);
	# Request is simple
	$self->{ready} = 1;
	return 0;
}

sub parse2 {
	my ($self, $line) = @_;

	$self->{'buf'.'HEADER2'} = $line;

	if (!($line =~ /^(\d+)\t{0,1}(.*)/)) {
		$self->{errstr} = 'Malformed request';
		return 0;
	}

	$self->clear;

	$self->{post_size} = $1;
	$self->{post_type} = $2;

	$self->{ready} = 1;

	return $self->{post_size};
}

sub parse3 {
	my ($self, $data, $n) = @_;

	if ($self->{post_fd}) { # Connected to an output stream
		# Write to it
		my $fh = $self->{post_fd};
		print $fh $data;
	} else {
		# Buffer
		$self->{'post_data'} .= $data;
	}
	$self->{'buf'.'SIZE'} += $n;

	if ($self->{'buf'.'SIZE'} >= $self->{post_size}) {
		$self->{ready} = $self->{ready} || 1;
		$self->{sent} = 1;
		$self->close;
	}

	return $self->{sent};
}

sub write_to {
	my ($self, $fh) = @_;

	# Save filehandle
	$self->{post_fd} = $fh;

	# Hack -- if we have something buffered, dump it
	print $fh $self->{'post_data'};
	$self->close if ($self->{sent});
}

sub post_file {
	my ($self, $filename, $_type) = @_;
	if (!-r $filename) {
		# Error
	}

	my $size = -s $filename;
	my $type = mimetype $filename || $_type;
	my $data = '';

	# Small file
	if ($size <= 1024) {
		my $fh;
		open $fh, $filename;
		binmode $fh;
		read $fh, $self->{post_data}, $size;
		close $fh;

		$self->{post_size} = $size;
		$self->{post_type} = $type;

		$self->{ready} = 1;
	}
	# Large file, stream it 
	else {
		my $fh;
		open $fh, $filename;
		binmode $fh;

		$self->{post_fd} = $fh;
		$self->{post_offset} = 0;
		$self->{post_size} = $size;
		$self->{post_type} = $type;
		
		$self->{ready} = 1;
	}
}

sub post_fd {
	my ($self, $fd, $size, $type) = @_;

	$self->{post_fd} = $fd;
	$self->{post_offset} = 0;
	$self->{post_size} = $size;
	$self->{post_type} = $type || '';

	$self->{ready} = 1;
}

sub close {
	my $self = shift;
	if (defined $self->{post_fd}) {
		close ($self->{post_fd});
		undef $self->{post_fd};
	}
}

sub clear {
	my $self = shift;
	$self->close();
	$self->{post_size} = 0;
	$self->{post_type} = '';
	$self->{post_data} = '';
}

sub post {
	my ($self, $data, $size, $type) = @_;

	$self->{post_type} = $type || $self->{post_type};
	$self->{post_size} = $size;
	$self->{post_data} = $data;

	$self->{ready} = 1;
}

sub print {
	my ($self, $fd) = @_;
	$fd = select if !defined $fd;

	my $complex = ($self->{search} || $self->{range} || $self->{post_fd} || $self->{post_data});

	# Request
	print $fd $self->{selector};
	print $fd $self->{search} . "\t" . $self->{range} if $complex;
	print $fd "\r\n";

	# Connected to a stream
	if ($self->{post_fd}) {
		# Header
		print $fd $self->{post_size} . "\t";
		print $fd $self->{post_type} ;
		print $fd "\r\n";

		# Redirect it
		my $buf = '';
		while (read $fd, $buf, 1024) {
			print $fd $buf;
		}
	}
	# Has data buffered
	elsif ($self->{post_data}) {
		# Header
		print $fd $self->{post_size} . "\t";
		print $fd $self->{post_type} ;
		print $fd "\r\n";

		# Body
		print $fd $self->{post_data};
	}
	# Need dummy
	elsif ($complex) {
		# Header
		print $fd $self->{post_size} . "\t";
		print $fd "\r\n";	
	}

}

sub DESTROY {
	$_[0]->close();
}

}#</package>

#
# RESPONSE
#
{#<package>
package TLSGopher::response;

sub new {
	my $self = {};

	$self->{selector} = '';

	$self->{body_size} = 0;
	$self->{body_type} = '';
	$self->{body_data} = '';

	$self->{raw} = 0;	# No need to apply header

	$self->{ready} = 0;	# Ready to send
	$self->{header_sent} = 0; # Header sent
	$self->{sent} = 0; # Body sent
	$self->{is_error} = 0;

	bless $self;
	return $self;
}

sub read_from {
	my ($self, $fd) = @_;
	$self->{body_fd} = $fd;

	# This could fail
	$fd->blocking(0);
}

sub error {
	my $self = shift;
	if (@_ == 0) { return $self->{is_error}; }
	my $menu = "";
	my $c = 'e';
	for (@_) {
		$menu .= $c."\t".$_."\r\n";
		$c = 'i';
	}
	$self->{body_data} = $menu;
	$self->{body_type} = 'text/x-menu';
	$self->{body_size} = length $menu;
	$self->{ready} = 1;
	$self->{is_error} = 1;
}
sub size {
	my $self = shift;
	if (@_ == 0) { return $self->{body_size}; }
	$self->{body_size} = shift;
}
sub type {
	my $self = shift;
	if (@_ == 0) { return $self->{body_type}; }
	$self->{body_type} = shift;
}
sub write {
	my $self = shift;
	for (@_) {	$self->{body_data} .= $_; }
	$self->{body_size} = length $self->{body_data};
}

sub print_header {
	my ($self, $fd) = @_;
	$fd = select if !defined $fd;

	# Streams should handle their own errors.
	if (!$self->{raw}) {
		print $fd $self->{body_size} . "\t";
		print $fd $self->{body_type} ;
		print $fd "\r\n";
	}

	$self->{header_sent} = 1;
}

sub print_body {
	my ($self, $fd) = @_;
	$fd = select if !defined $fd;

	# Buffered body
	print $fd $self->{body_data};

	# Connected to an input stream
	if ($self->{body_fd}) {
		# Copy a portion of it
		my $buf = '';
		my $n;
		if (($n = read $self->{body_fd}, $buf, 1024)) {
			print $fd $buf;
		}
		if (!$n && !$!{EAGAIN}) {
		#if (eof $self->{body_fd}) { # :(
			$self->{sent} = 1;
		}
		# Hack -- clean buffered body
		$self->{body_data} = '';
	}
	# Not connected
	else {
		# All we had was buffered body
		$self->{sent} = 1;
	}

}

sub print {
	my ($self, $fd) = @_;
#	$fd = select if !defined $fd;

	# Header
	$self->print_header($fd) unless $self->{header_sent};

	# Body
	$self->print_body($fd) unless $self->{sent};
}

sub sent {
	my ($self) = @_;
	if ($self->{track_pid} && $self->{sent}) {
		warn "Waiting for child process to finish :(\n";
		waitpid $self->{track_pid}, 0;
		warn "Ok\n";
	}
	return $self->{sent};
}
sub raw { 
	$_[0]->{raw} = $_[1] if (defined $_[1]); 
	return $_[0]->{raw}; 
}

sub track_pid {
	my ($self, $pid) = @_;
	$self->{track_pid} = $pid;
}

sub DESTROY {
	my ($self) = @_;
	if ($self->{body_fd}) {
		close $self->{body_fd};
	}
}

}#</package>

#
# CLIENT CONNECTION
#
{#<package>
package TLSGopher::connection;

use constant {
    HEADER1	=> 'Request',
    HEADER2	=> 'Header',
    BINDATA	=> 'Data',
    IGNORE	=> 'Ignore',
};

sub new {

	my ($class, $sock) = @_;
	my $self = {};

	$sock->blocking(0);

	$self->{sock} = $sock;
	$self->{binmode} = 0;
	$self->{method} = HEADER1;

	$self->{next_request} = undef;

#	warn "connection opened ($sock).\n";
	
	$self->{queue} = ();

	# Save cert meta-data	
	my ($subject_name, $issuer_name);
	if (ref($sock) eq "IO::Socket::SSL") {
		$subject_name = $sock->peer_certificate("subject");
		$issuer_name = $sock->peer_certificate("issuer");
	}
	$self->{TLSsubject} = $subject_name;
	$self->{TLSissuer} = $issuer_name;

	bless $self;
	return $self;
}

sub cycle {
	my $self = shift;
	
	$self->handle_queue;

	if ($self->{binmode}) {

		my $need = 1024;

		if ($self->{binmode} > 0 && $self->{binmode} < $need) { 
			$need = $self->{binmode};
		}

		# Read binary portion
		my $data;
		my $n = read $self->{sock}, $data, $need;
		if (!$n && !$!{EAGAIN}) { return 0; } # Socket disconnected

		# React
		if (defined $n) {

			$self->{read_cb}($self, $data, $n);

			if ($n >= 1) {
				# Old request, cont..
				$self->{binmode} -= $n;
				$self->{next_request}->parse3($data, $n);
			}
		}

	} else {
		# Read one line
		my $sock = $self->{sock};
		my $line = <$sock>;
		if (!$line && !$!{EAGAIN}) { return 0; } # Socket disconnected

		# React
		if ($line) {

			$self->{read_cb}($self, $line, length $line);

			if (!defined $self->{next_request}) {
				# New request
				$self->{next_request} = new TLSGopher::request;
				$self->{next_request}->parse1($line);
			} else {
				# New request, cont..
				my $size = $self->{next_request}->parse2($line);
				$self->{binmode} = $size;
			}
		}
	}
	
	# React to current request 
	if (defined $self->{next_request}) {

		# Request has failed
		if ($self->{next_request}->{errstr}) {
			# Disconnect
			$self->{sock}->close();
		}
		# Request is ready
		elsif ($self->{next_request}->{ready} == 1) {
			# Resolve
			$self->handle_request( $self->{next_request} );
			$self->{next_request}->{ready} = 2; # Hack, do not repeat
		}
		# Request is resolved
		if ($self->{next_request}->{sent}) {
			# Delete
			undef $self->{next_request} ;
		}
	}

	return (defined $self->{sock}->connected() ? 1 : 0);
}

sub handle_queue {
	my $self = shift;

	# Queue is empty
	if ($#{$self->{queue}} <= -1) { return; }

	# Get first response in queue
	my $resp = @{$self->{queue}}[0];

	#print "Streaming $resp->{body_fd} > CLIENT\n";

	# Advance it
	$resp->print( $self->{sock} );

	# Remove it when done
	if ($resp->sent) {
		shift (@{$self->{queue}});	
	}
}

sub handle_request {
	my ($self, $req) = @_;

	my $resp = new TLSGopher::response; 

	$req->{response} = $resp;
	$resp->{request} = $req;
	$resp->{selector} = $req->{selector};

	my $stop = $self->{request_cb}($self, $resp, $req);

# This allows user implementation to CANCEL response wholetogether.
# 	This is not recommended, as such behavior will break clients
#	and a simple 0\r\n is always much better.
#	if (!$stop) {  
		push @{$self->{queue}}, $resp;
#	}

}

sub close {
	my $self = shift;
	close( $self->{sock} ); 
#	warn "\t connection closed.\n";
}

}#</package>

#
# SERVER
#
{#<package>
package TLSGopher::server;

my ($errstr) = ('');

sub new {
	my $self = {};
# if 0
#	my ($class, %cfg) = @_;
# else
	my ($class, $cfg_ref) = @_;
	my %cfg = %{$cfg_ref};
	undef $cfg_ref;
# endif

	$self->{accept_cb} =
	$self->{request_cb} =
	$self->{read_cb} =
	$self->{close_cb} = sub { };

	$self->{config} = %cfg;
	$self->{conns} = [];

	$IO::Socket::SSL::DEBUG = $cfg{'Debug'} ? 1 : 0;

	if ($cfg{'TLSoff'}) {

		warn "TLS is off!";
		$self->{sock} = IO::Socket::INET->new( 

			Listen	=> 5,
			LocalAddr => $cfg{'Host'},
			LocalPort => $cfg{'Port'},
			Proto     => 'tcp',
			Reuse     => 1,

		);
		if (!$self->{sock}) { $errstr = "Can't bind $cfg{'Host'} $cfg{'Port'} " }
	} else {
		$self->{sock} = IO::Socket::SSL->new( 

			Listen	=> 5,
			LocalAddr => $cfg{'Host'},
			LocalPort => $cfg{'Port'},
			Proto     => 'tcp',
			Reuse     => 1,
			#   SSL_verify_mode => 0x01,
			SSL_passwd_cb => $cfg{'TLSpassphrase'} ? 
			   		sub {return $cfg{'TLSpassphrase'}} : undef,
			SSL_key_file => $cfg{'TLSkey'},
			SSL_cert_file => $cfg{'TLScert'},

		);
		if (!$self->{sock}) { $errstr = IO::Socket::SSL::errstr; }
	}

	if (!$self->{sock}) {
		return undef;
	}

	$self->{sock}->blocking(0);
	$self->{stop} = 0;

	bless $self;
	return $self;
}

sub errstr { return $errstr; }

sub stop {
	my $self = shift;
	$self->{stop} = 1;

	#warn "stopping server.\n";
}

sub listen {
	my $self = shift;

	#warn "waiting for connections.\n";

	while (!$self->{stop}) {

		$self->cycle;
		$self->wait;

	}

	$self->{sock}->close();
}

sub cycle {
	my $self = shift;

	while((my $s = $self->{sock}->accept())) {

		my $conn = new TLSGopher::connection $s;

		$self->{accept_cb}($conn, $self);

		$conn->{read_cb} = $self->{read_cb};
		$conn->{request_cb} = $self->{request_cb};

		push @{$self->{conns}}, $conn;	# Add connection
	}

	for (my $i = 0; $i < $#{$self->{conns}} + 1; $i++) {
		my $conn = $self->{conns}[$i];
		if (!$conn->cycle) {
			$self->{close_cb}($@{$self->{conns}}[$i], $self);
    		splice @{$self->{conns}}, $i, 1;
    		$i--;
    	}
    }
}

sub wait {

	# TODO: select on file descriptors

}

sub register {
	my ($self, %cfg) = @_;
	for (%cfg) {
		$self->{$_.'_cb'} = $cfg{$_};
	}
}

}#</package>

#
# CLIENT
#
{#<package>
package TLSGopher::client;

my ($sock);

sub new {

} 

}#</package>

1;
