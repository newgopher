#!/usr/bin/perl

$buffer = "Your SELECTOR is `" . $ENV{'GOPHER_SELECTOR'} . "'\n";
$buffer .= "You have sent ".$ENV{'CONTENT_SIZE'} . " bytes " . 
          "of content type `".$ENV{'CONTENT_TYPE'}. "' !\n";

$mimetype = 'text/plain';

$size = length $buffer;
print $size."\t".$mimetype."\r\n";
print $buffer;

