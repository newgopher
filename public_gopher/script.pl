#!/usr/bin/perl

if ($ENV{"QUERY_STRING"}) {
	# Request
	$cookie = $ENV{"QUERY_STRING"};
	$data_size = $ENV{"CONTENT_SIZE"} || 1024;
	$data_read = 0;

	$data_read = read(STDIN, $data, $data_size);

	$target = 's'.$ENV{"GOPHER_SELECTOR"}."\t"."gadl"."\t\t\r\n";
	$buffer = "";

	@lines = split /\n/, $data;
	
	foreach $line (@lines) {
		
		($key, $val) = split /:/, $line;
		$key =~ s/\s//;
		$val =~ s/^\s*//;

		$key =~ tr/A-Z/a-z/;

		$buffer .= " FORM [$key] = $val \n";	
	}

	$buffer .= "Thank you for your submission! \r\n";
	#$buffer .= $data;

	$mimetype = 'text/plain';
} else {
	# Form
	$cookie = rand();

	$target = 's'.$ENV{"GOPHER_SELECTOR"}."\t".$cookie."\t\t\r\n";
	$buffer = "";

	$buffer .= $target;
	$buffer .= "Username: \nPassword: \nE-Mail: \n";

	$mimetype = 'application/x-interactive';
}

$size = length $buffer;
print $size."\t".$mimetype."\r\n";
print $buffer;

